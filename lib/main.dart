import 'package:belajar_mvvm/Widgets/Profile%20Widget/editProfile.dart';
import 'package:belajar_mvvm/Widgets/dashboardWidget.dart';
import 'package:belajar_mvvm/Widgets/eventDetailsWidget.dart';
import 'package:belajar_mvvm/Widgets/loginWidget.dart';
import 'package:belajar_mvvm/Widgets/profileWidget.dart';
import 'package:belajar_mvvm/Widgets/registerWidget.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter/material.dart';
import 'Widgets/loginRegisterWidget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EventEvent',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Proxima',
        primarySwatch: eventajaGreen,
        backgroundColor: Colors.white
      ),
      home: new LoginRegisterWidget(),
      routes: <String, WidgetBuilder>{
        '/LoginRegister': (BuildContext context) => LoginRegisterWidget(),
        '/Login': (BuildContext context) => LoginWidget(),
        '/Register': (BuildContext context) => RegisterWidget(),
        '/Dashboard': (BuildContext context) => DashboardWidget(),
        '/Profile': (BuildContext context) => ProfileWidget(),
        '/EventDetails': (BuildContext context) => EventDetailsConstructView(),
        '/EditProfile': (BuildContext context) => EditProfileWidget()
      },
    );
  }
}
