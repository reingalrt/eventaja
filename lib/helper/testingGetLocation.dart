import 'dart:async';

import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';

class ListenPage extends StatefulWidget {
  @override
  _ListenPageState createState() => _ListenPageState();
}

class _ListenPageState extends State<ListenPage> {

  Location location = new Location();

  Map<String, double> currentLocation = new Map();
  StreamSubscription<Map<String, double>> locationSubcription;
  String err;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    currentLocation['latitude'] = 0.0;
    currentLocation['longitude'] = 0.0;

    initPlatformState();
    locationSubcription = location.onLocationChanged().listen((Map<String, double> result){
      setState(() {
        currentLocation = result;
      });
    });
    
      }
    
      @override
      Widget build(BuildContext context) {
        return Scaffold(
          body: Center(child: Text('Get Longitude Latitude | Lat: ${currentLocation['latitude']} Long: ${currentLocation['longitude']}'),),
        ); 
      }
    
      void initPlatformState() async {
        Map<String, double> myLocation;
        try{
          myLocation = await location.getLocation();
          err = "";
        }on PlatformException catch(e){
          if(e.code == "PERMISSION_DENIED"){
            err = 'Permission Denied';
          }
          else if(e.code == 'PERMISSION_DENIED_NEVER_ASK'){
            err = 'Permission denied - please ask the user to enable location service';
          }
          myLocation = null;
        }
        setState(() {
          currentLocation = myLocation;
        });
      }
}