import 'dart:core';

import 'package:belajar_mvvm/Widgets/loginRegisterWidget.dart';
import 'package:belajar_mvvm/helper/API/registerModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'baseApi.dart';
import 'dart:async';
import 'package:belajar_mvvm/Widgets/dashboardWidget.dart';
import 'package:belajar_mvvm/helper/sharedPreferences.dart';

final String apiKey = '47d32cb10889cbde94e5f5f28ab461e52890034b';

String cookies = '';

Future<Register> requestRegister(BuildContext context, String username, String email, String password, GlobalKey<ScaffoldState> _scaffoldKey) async {
  final registerApiUrl = BaseApi().apiUrl + '/signup/register';

  Map<String, String> body = {
    'username': username,
    'email': email,
    'password': password,
    'gender': "null",
    'X-API-KEY': apiKey,
    'pictureAvatarURL': "male.jpg"
  };

  final response = await http.post(
    registerApiUrl,
    body: body,
    headers: {'Authorization': "Basic YWRtaW46MTIzNA=="}
  );

  print(response.statusCode);

  final myResponse =json.decode(response.body);

  if(response.statusCode == 201){
    final responseJson =jsonDecode(response.body);

    SharedPrefs().saveCurrentSession(response, responseJson);
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => DashboardWidget()));
    return Register.fromJson(responseJson);
  }
  else if(response.statusCode == 400){
    final responseJson = jsonDecode(response.body);
    //Register registerModel = new Register.fromJson(responseJson);

    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.red,
      duration: Duration(seconds: 3),
      content: Text(responseJson['desc'], style: TextStyle(color: Colors.white),),
    ));
  }
  else if(myResponse.containsKey('username')){
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.red,
      duration: Duration(seconds: 3),
      content: Text('Username already taken', style: TextStyle(color: Colors.white),),
    ));
  }
}

requestLogout(BuildContext context) async {
  final logoutApiUrl = BaseApi().apiUrl + '/signout';

  Map<String, String> body = {
    'X-API-KEY': apiKey
  };

  SharedPreferences prefs = await SharedPreferences.getInstance();
  
  final response = await http.post(
    logoutApiUrl,
    body: body,
    headers: {'Authorization': "Basic YWRtaW46MTIzNA==", 'cookie': prefs.getString('Session')}
  );

  print(response.statusCode);

  if(response.statusCode == 200){
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginRegisterWidget()));
    prefs.clear();
    print(prefs.getKeys());
  }
}