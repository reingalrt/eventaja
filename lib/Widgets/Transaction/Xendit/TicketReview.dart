import 'dart:convert';

import 'package:belajar_mvvm/Widgets/RecycleableWidget/WaitTransaction.dart';
import 'package:belajar_mvvm/Widgets/Transaction/Alfamart/WaitingTransactionAlfamart.dart';
import 'package:belajar_mvvm/Widgets/Transaction/BCA/InputBankData.dart';
import 'package:belajar_mvvm/Widgets/Transaction/GOPAY/WaitingGopay.dart';
import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';
import 'package:url_launcher/url_launcher.dart';

import '../CC.dart';

class TicketReview extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TicketReviewState();
  }
}

class _TicketReviewState extends State<TicketReview>{

  final promoCodeController = TextEditingController();

  String thisEventName;
  String thisEventImage;
  String thisTicketName;
  String thisEventAddres;
  String thisEventDate;
  String thisEventStartTime;
  String thisEventEndTime;
  String thisTicketAmount;
  String thisTicketPrice;
  String thisTicketFee;
  String expDate;
  Map<String, dynamic> paymentData;
  Map<String, dynamic> promoData;
  int pajak;
  int total;

  var uuid = new Uuid();

  Future getEventDetails() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var eventName = preferences.getString('EventName');
    var eventImage = preferences.getString('EventImage');
    var ticketName = preferences.getString('TicketName');
    var eventAddress = preferences.getString('EventAddress');
    var eventDate = preferences.getString('EventDate');
    var eventStartTime = preferences.getString('EventStartTime');
    var eventEndTime = preferences.getString('EventEndTime');
    var ticketAmount = preferences.getString('ticket_many');
    var ticketPrice = preferences.getString('ticket_price_total');
    var ticketFee = preferences.getString('ticket_fee');

    setState(() {
      thisEventName = eventName;
      thisEventImage = eventImage;
      thisTicketName = ticketName;
      thisEventAddres = eventAddress;
      thisEventDate = eventDate;
      thisEventStartTime = eventStartTime;
      thisEventEndTime = eventEndTime;
      thisTicketAmount = ticketAmount;
      thisTicketPrice = ticketPrice;
      thisTicketFee = ticketFee;
      pajak = int.parse(thisTicketFee);
      total = int.parse(thisTicketPrice) + pajak;
    });
  }

  Future getPaymentData(String expired) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('expDate', expired);

    var expiredDate = preferences.getString('expDate');

    expDate = expiredDate;
    print(expDate);

  } 

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEventDetails();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.9),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: GestureDetector(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back_ios, size: 35, color: eventajaGreenTeal,),
        ),
        centerTitle: true,
        title: Text('REVIEW TICKET', style: TextStyle(color: eventajaGreenTeal),),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: (){
          postPurchaseTicket();
        },
        child: Container(
                  height: 50,
                  color: Colors.deepOrangeAccent,
                  child: Center(
                    child: Text(
                      'PURCHASE',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  )),
      ),

      body: ListView(
        children: <Widget>[
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(15),
            height: 280,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget> [
                Center(
                  child: Text(thisEventName == null ? '' : thisEventName, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 20),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 150,
                    width: 100,
                    child: Image(image: thisEventImage == null ? AssetImage('assets/white.png') : NetworkImage(thisEventImage), fit: BoxFit.fill)
                  ),
                  SizedBox(width: 20),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(thisTicketAmount == null ? '' : thisTicketAmount + 'X' + ' Ticket(s)'),
                      SizedBox(height: 15),
                      Text(thisTicketName == null ? '' : thisTicketName, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                      SizedBox(height: 15),
                      Container(
                        width: 190,
                        child: Text(thisEventAddres == null ? '' : thisEventAddres, overflow: TextOverflow.ellipsis)
                      ),
                      SizedBox(height: 15),
                      Text(thisEventDate == null ? '' : thisEventDate),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment:CrossAxisAlignment.start,
                        children: <Widget> [
                          Text(thisEventStartTime == null ? '' : thisEventStartTime + ' - ' + thisEventEndTime == null ? '' : thisEventEndTime)
                        ]
                      )
                    ]
                  )
                ]
              )
              ]
            )
          ),
          SizedBox(height: 50),
          Container(
            padding: EdgeInsets.all(15),
            color: Colors.white,
            height: 150,
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget> [
                Text('Input Promo Code'),
                TextFormField(
                  controller: promoCodeController,
                  decoration: InputDecoration(
                    hintText: 'Example: TIX25'
                  )
                ),
                SizedBox(height: 15),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  child: RaisedButton(
                    child: Text('APPLY', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                    onPressed: promoCodeController.text == "" || promoCodeController == null ? (){} : (){
                      postPromoCode();
                    },
                    color: promoCodeController.text == "" || promoCodeController == null ? eventajaGreenTeal.withOpacity(0.2) : eventajaGreenTeal
                  )
                )
              ]
            )
          ),
          SizedBox(height: 50),
          Container(
            padding: EdgeInsets.all(15),
            color: Colors.white,
            height: 150,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget> [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Ticket Price'),
                    SizedBox(width: 95),
                    Text(':'),
                    SizedBox(width: 50),
                    Text('Rp. ' + thisTicketPrice)
                  ]
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Processing Fee'),
                    SizedBox(width: 70.5),
                    Text(':'),
                    SizedBox(width: 50),
                    Text(pajak.toString())
                  ]
                ),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.centerRight,
                  child: Divider(
                    height: 10,
                    indent: 150
                  )
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Total'),
                    SizedBox(width: 135),
                    Text(':'),
                    SizedBox(width: 50),
                    Text('Rp. ' + total.toString(), style: TextStyle(color: eventajaGreenTeal, fontWeight: FontWeight.bold, fontSize: 20), textAlign: TextAlign.end,)
                  ]
                ),
              ]
            )
          )
        ],
      ),
    );
  }

  Future postPromoCode() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    var session;

    setState((){
      session = preferences.getString('Session');
    });

    Map<String, dynamic> body = {
      'X-API-KEY': API_KEY,
      'code': promoCodeController.text,
      'quantity': preferences.getString('ticket_many'),
      'ticketID': preferences.getString('TicketID'),
      'paymentID': preferences.getString('payment_method_id')
    };

    String url = BaseApi().apiUrl + '/promo/check';
    final response = await http.post(
      url,
      headers: {
        'Authorization': AUTHORIZATION_KEY,
        'cookie': session
      },
      body: body
    );

    print(response.statusCode);
    print(response.body);
    print(preferences.getString('TicketID'));

    if(response.statusCode == 200){
      print(response.body);
      setState((){
        var extractedJson = json.decode(response.body);
        promoData = extractedJson;

        total = int.parse(promoData['data']['amount_detail']['price_after_discount']);
      });
    }
  }

  Future<Null> postPurchaseTicket() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var session;

    setState((){
      session = prefs.getString('Session');
    });

    Map<String, dynamic> body = {
      'X-API-KEY': API_KEY,
      'ticketID': prefs.getString('TicketID'),
      'quantity': prefs.getString('ticket_many'),
      'firstname': prefs.getString('ticket_about_firstname'),
      'lastname': prefs.getString('ticket_about_lastname'),
      'email': prefs.getString('ticket_about_email'),
      'phone': prefs.getString('ticket_about_phone'),
      'note': prefs.getString('ticket_about_aditional'),
      'payment_method_id': prefs.getString('payment_method_id'),
      'identifier': uuid.v4().toString()
    };

    String purchaseUri = BaseApi().apiUrl + '/ticket_transaction/post';
    final response = await http.post(
      purchaseUri, 
      headers: {
        'Authorization': AUTHORIZATION_KEY,
        'cookie': session
      },
      body: body
    );

    print(response.statusCode);

    if(response.statusCode == 200){
      print('mantab gan');
      print(response.body);
      var extractedData = json.decode(response.body);
      setState(() {
        paymentData = extractedData['data'];
        print(paymentData['expired_time']);
        getPaymentData(paymentData['expired_time']);
      });
      if(paymentData['payment_method_id'] == '1'){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CreditCardInput(
          transactionID: paymentData['id'],
          expDate: paymentData['expired_time'],
        )));
      }
      else if(paymentData['payment_method_id'] == '4'){
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (BuildContext context) => WaitingGopay(
              amount: paymentData['amount'],
              deadline: paymentData['expired_time'],
              gopaytoken: paymentData['gopay'],
              expDate: paymentData['expired_time'],
              transactionID: paymentData['id'],
            )), 
            ModalRoute.withName('/Dashboard'));
      }
      else if(paymentData['payment_method_id'] == '2'){
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => WaitTransaction(expDate: paymentData['expired_time'], transactionID: paymentData['id'], finalPrice: total.toString())), 
            ModalRoute.withName('/Dashboard'));
      }
      else if(paymentData['payment_method_id'] == '3'){
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => WaitingTransactionAlfamart(transactionID: paymentData['id'], expDate: paymentData['expired_time'],)), ModalRoute.withName('/Dashboard'), );
      }
      else if(paymentData['payment_method_id'] == '5'){
        launch(paymentData['payment']['data_vendor']['payment_url']);
      }
      else if(paymentData['payment_method_id'] == '9'){
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => WaitTransaction(expDate: paymentData['expired_time'], transactionID: paymentData['id'], finalPrice: total.toString())), 
            ModalRoute.withName('/Dashboard'));
      }
      else if(paymentData['payment_method_id'] == '7'){
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => PaymentBCA(expDate: paymentData['expired_time'], transactionID: paymentData['id'],)), ModalRoute.withName('/Dashboard'));
      }
      
    }
  }
}