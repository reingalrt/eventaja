import 'dart:convert';
import 'dart:io';

import 'package:belajar_mvvm/Widgets/dashboardWidget.dart';
import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:belajar_mvvm/helper/API/loginModel.dart';
import 'package:belajar_mvvm/helper/sharedPreferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:belajar_mvvm/helper/API/apiHelper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginWidgetState();
  }
}

class _LoginWidgetState extends State<LoginWidget> {
  
  var _scaffoldKey                                = GlobalKey<ScaffoldState>(); ///Buat GlobalKey Scaffold agar bisa dipanggil di Class lain
  final TextEditingController _usernameController = TextEditingController();    ///Buat controller untuk mengambil dan menyimpan input username
  final TextEditingController _passwordController = TextEditingController();    ///Buat controller untuk mengambil dan menyimpan input password

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CupertinoNavigationBar(
        padding:
            EdgeInsetsDirectional.only(start: 15, bottom: 10, end: 15, top: 5),
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: () {
            backEvent();
          },
          child: Icon(Icons.arrow_back, color: eventajaGreenTeal),
        ),
        middle: Text(
          'Login',
          style: TextStyle(fontSize: 20, color: eventajaGreenTeal),
        ),
      ),
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 40, right: 40, top: 45),
            child: Material(
              color: Colors.white,
              child: loginForm(),
            ),
          ),
        ],
      ),
    );
  }


  ///Buat fungsi tombol back
  bool backEvent() {
    return Navigator.pop(context);
  }


  ///Construct LoginForm Widget
  Widget loginForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        TextFormField(
          controller: _usernameController,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Username',
          ),
        ),
        SizedBox(
          height: 15,
        ),
        TextFormField(
          controller: _passwordController,
          keyboardType: TextInputType.text,
          autofocus: false,
          obscureText: true,
          decoration: InputDecoration(
            border: InputBorder.none,
            // fillColor: Colors.white,
            // filled: true,
            hintText: 'Password',
          ),
        ),
        SizedBox(
          height: 40,
        ),
        ButtonTheme(
          minWidth: 500,
          height: 50,
          child: RaisedButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Text('Login',
                style: TextStyle(fontSize: 15, color: Colors.white)),
            color: eventajaGreenTeal,
            onPressed: () {
              if (_usernameController.text.length == 0 ||
                  _usernameController.text == null) {
                _scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.red,
                    content: Text(
                      'Email / Username cannot be empty',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                );
              } else if (_passwordController.text.length == 0 ||
                  _passwordController.text == null) {
                _scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.red,
                    content: Text(
                      'Password connot be empty',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                );
              } else {
                requestLogin(context, _usernameController.text,
                    _passwordController.text, _scaffoldKey);
              }
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20),
        ),
        GestureDetector(
          child: Text('Forgot Your Password?',style: TextStyle(fontSize: 15),),
          onTap: (){},
        ),
        SizedBox(
          height: 25,
        ),
        Divider(height: 15,)
      ],
    );
  }


  ///Untuk fetching RequestLogin
  Future<LoginModel> requestLogin(BuildContext context, String username, String password, GlobalKey<ScaffoldState> _scaffoldKey) async {
  final loginApiUrl = BaseApi().apiUrl + '/signin/login?=';
  final String apiKey = '47d32cb10889cbde94e5f5f28ab461e52890034b';

  Map<String, String> body = {
    'username': username,
    'password': password,
    'X-API-KEY': apiKey
  };

  final response = await http.post(
    loginApiUrl,
    headers: {HttpHeaders.authorizationHeader: "Basic YWRtaW46MTIzNA=="},
    body: body
  );

  print('status code: ' + response.statusCode.toString());
  print(response.body);

  ///Jika statusCode == 200 maka lanjutkan proses dan alihkan ke halaman berikutnya

  if(response.statusCode == 200){
    SharedPreferences prefs =await SharedPreferences.getInstance();
    print('apiHelper-line41:' + cookies);
    print('username: '+prefs.getString('Last Username').toString());
    print('id: ' + prefs.getString('Last User ID').toString());
    final responseJson = json.decode(response.body);

    ///simpan sesi saat ini
    setState(() {
      prefs.setString('Session', response.headers['set-cookie']);
    });


    SharedPrefs().saveCurrentSession(response, responseJson);
    Navigator.pushReplacementNamed(context, '/Dashboard');
    return LoginModel.fromJson(responseJson);
  } ///Jika statusCode == 400 maka munculin Snackbar error Belum teregister / salah input akun
  else if(response.statusCode == 400){
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.red,
      duration: Duration(seconds: 3),
      content: Text('You\'re not registered!', style: TextStyle(color: Colors.white),),
    ));
  }///Jika statusCode == 408 maka munculin Snackbar error Request Timeout!
  else if(response.statusCode == 408){
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.red,
      duration: Duration(seconds: 3),
      content: Text('Request Timeout! please retry submiting data'),
    ));
  }///Else, simpan sessi gagal
  else{
    final responseJson = json.decode(response.body);
    SharedPrefs().saveCurrentSession(response, responseJson);
  }
  print(LoginModel().description);
}
}
