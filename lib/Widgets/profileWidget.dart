import 'dart:convert';

import 'package:belajar_mvvm/Widgets/dashboardWidget.dart';
import 'package:belajar_mvvm/Widgets/editProfileWidget.dart';
import 'package:belajar_mvvm/helper/API/apiHelper.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:belajar_mvvm/helper/API/profileModel.dart';
import 'package:http/http.dart' as http;
import 'package:belajar_mvvm/helper/API/apiHelper.dart' as apiHelper;
import 'package:shared_preferences/shared_preferences.dart';
import 'Profile Widget/profileHeader.dart';
import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:http/http.dart' as http;

class ProfileWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfileWidgetState();
  }
}

class _ProfileWidgetState extends State<ProfileWidget> {
  String fullName;
  String firstName;
  String username;
  String pictureUri;
  String userId;
  String email;
  String phone;
  String website;
  String eventCreated;
  String eventGoing;
  String follower;
  String following;
  String lastName;
  String bio;

  List userData;

  @override
  void initState() {
    super.initState();
    getUserProfileData();
  }

  @override
  Widget build(BuildContext context) {
    return ProfileHeader(
      username: username,
      fullName: fullName,
      firstName: firstName,
      email: email,
      phone: phone,
      website: website,
      profilePhotoURL: pictureUri,
      currentUserId: userId,
      eventCreatedCount: eventCreated,
      eventGoingCount: eventGoing,
      follower: follower,
      following: following,
      lastName: lastName,
      bio: bio,
    );
  }

  Future getUserProfileData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString('Last User ID');
    var session = preferences.getString('Session');

    print(userId);

    final userProfileAPI = BaseApi().apiUrl +
        '/user/detail?X-API-KEY=' +
        API_KEY +
        '&userID=' +
        userId;
    print(userProfileAPI);
    final response = await http.get(userProfileAPI, headers: {
      'Authorization': 'Basic YWRtaW46MTIzNA==',
      'cookie': session
    });

    print(response.statusCode);

    if (response.statusCode == 200) {
      setState(() {
        var extractedData = json.decode(response.body);
        userData = extractedData['data'];
        username = userData[0]['username'];
        firstName = userData[0]['firstName'];
        email = userData[0]['email'];
        phone = userData[0]['phone'];
        website = userData[0]['website'];
        fullName = userData[0]['fullName'];
        lastName = userData[0]['lastName'];
        pictureUri = userData[0]['pictureFullURL'];
        eventCreated = userData[0]['countEventCreated'];
        eventGoing = userData[0]['countEventGoing'];
        following = userData[0]['countFollowing'];
        follower = userData[0]['countFollower'];
        bio = userData[0]['shortBio'];
      });
    }
  }
}
