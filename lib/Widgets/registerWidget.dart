import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:belajar_mvvm/helper/API/apiHelper.dart';

class RegisterWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RegisterWidgetState();
  }
}

class _RegisterWidgetState extends State<RegisterWidget> {
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: CupertinoNavigationBar(
        padding:
            EdgeInsetsDirectional.only(start: 15, bottom: 10, end: 15, top: 5),
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: () {
            backEvent();
          },
          child: Icon(Icons.arrow_back, color: eventajaGreenTeal),
        ),
        middle: Text(
          'Register',
          style: TextStyle(fontSize: 20, color: eventajaGreenTeal),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 40, right: 40, top: 45),
            child: Material(
              color: Colors.white,
              child: registerForm(),
            ),
          )
        ],
      ),
    );
  }

  bool backEvent() {
    return Navigator.pop(context);
  }

  Widget registerForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        TextFormField(
          controller: _usernameController,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              hintText: 'Username',
              border: InputBorder.none
          ),
        ),
        SizedBox(
          height: 15,
        ),
        TextFormField(
          controller: _emailController,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              hintText: 'Email',
              border: InputBorder.none
          ),
        ),
        SizedBox(
          height: 15,
        ),
        TextFormField(
          controller: _passwordController,
          keyboardType: TextInputType.text,
          autofocus: false,
          obscureText: true,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              hintText: 'Password',
              border: InputBorder.none),
        ),
        SizedBox(height: 15),
        ButtonTheme(
          minWidth: 500,
          height: 50,
          child: RaisedButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Text('Register',
                style: TextStyle(fontSize: 15, color: Colors.white)),
            color: eventajaGreenTeal,
            onPressed: () {
              Pattern pattern =
                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
              RegExp regex = new RegExp(pattern);
              if (_usernameController.text.length == 0 ||
                  _usernameController.text == null ||
                  _emailController.text.length == 0 ||
                  _passwordController.text.length == 0) {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    'Please check your input',
                    style: TextStyle(color: Colors.white),
                  ),
                  backgroundColor: Colors.red,
                ));
              } else if (!regex.hasMatch(_emailController.text)) {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                    'Invalid Email Format',
                    style: TextStyle(color: Colors.white),
                  ),
                ));
              } else if (_passwordController.text.length < 8) {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    'Password at least 8 characters',
                    style: TextStyle(color: Colors.white),
                  ),
                  backgroundColor: Colors.red,
                ));
              } else {
                requestRegister(
                    context,
                    _usernameController.text,
                    _emailController.text,
                    _passwordController.text,
                    _scaffoldKey);
              }
            },
          ),
        ),
        SizedBox(height: 15,),
        Divider(height: 15,)
      ],
    );
  }
}
