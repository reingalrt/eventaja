import 'dart:convert';

import 'package:belajar_mvvm/Widgets/Transaction/SelectTicket.dart';
import 'package:belajar_mvvm/Widgets/Transaction/SuccesPage.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:belajar_mvvm/helper/static_map_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:marquee/marquee.dart';
//import 'package:belajar_mvvm/helper/MarqueeWidget.dart';
import 'package:marquee_flutter/marquee_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/cupertino.dart';

class EventDetailsConstructView extends StatefulWidget {
  final Map<String, dynamic> eventDetailsData;
  EventDetailsConstructView({Key key, @required this.eventDetailsData})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EventDetailsConstructViewState();
  }
}

class _EventDetailsConstructViewState extends State<EventDetailsConstructView>
    with AutomaticKeepAliveClientMixin<EventDetailsConstructView> {
  ScrollController _scrollController = new ScrollController();
  String creatorFullName;
  String creatorName;
  String creatorImageUri;
  String startTime;
  String endTime;
  String isGoing;
  String address;
  String lat;
  String long;
  String isPrivate = '0';
  String ticketTypeURI = "";
  String ticketPrice = "";
  String isLoved = "0";
  String email = 'assets/icons/btn_email.png';
  String phoneNumber = 'assets/icons/btn_phone.png';
  String website = 'assets/icons/btn_web.png';
  String loveBtn = 'assets/icons/btn_love_small.png';
  String eventID;

  int loveCount = 0;
  int loveClickedCount = 0;

  bool ticketPriceStringVisibility;
  bool isTicketUnavailable;
  bool isLoveClicked = false;

  Map<String, dynamic> detailData;
  Map<String, dynamic> invitedData;
  Map<String, dynamic> ticketType;
  Map<String, dynamic> ticketStat;
  List goingData;
  List invitedUserList;

  var session;

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    testGetData();
    getEventDetailsSpecificInfo();
    getInvitedUser();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[customAppbar(), bannerDetails()],
        ),
      ),
    );
  }

  Widget customAppbar() {
    return Container(
        height: 65,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: UnconstrainedBox(
                alignment: Alignment.topCenter,
                constrainedAxis: Axis.horizontal,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 250,
                  color: Colors.grey,
                ),
              ),
            ),
            Positioned(
              top: 16,
              left: 15,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop(true);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2,
                  ),
                  GestureDetector(
                      child: Icon(
                    Icons.person_add,
                    size: 30,
                    color: Colors.white,
                  )),
                  SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                    child: Icon(
                      Icons.share,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                    child: Icon(
                      Icons.more_vert,
                      color: Colors.white,
                      size: 30,
                    ),
                  )
                ],
              ),
            )
          ],
        ));
  }

  Widget bannerDetails() {
    return Expanded(
      child: NotificationListener(
        onNotification: (t) {
          if (t is ScrollUpdateNotification) {
//            print(_scrollController.position.pixels);
            _scrollController.offset.clamp(0, 355);
            // if (_scrollController.position.pixels <= 355.0) {

            // }
          }
        },
        child: ListView(
          controller: _scrollController,
          shrinkWrap: true,
          children: <Widget>[
            Stack(children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / .5,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    UnconstrainedBox(
                      alignment: Alignment.topCenter,
                      constrainedAxis: Axis.horizontal,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 230,
                            width: 500,
                            color: Colors.grey.withOpacity(0.5),
                            // child: Image.network('https://ichef.bbci.co.uk/news/660/cpsprodpb/1723B/production/_105097749_dildoedit.png',
                            //   fit: BoxFit.cover,
                            // ),
                          ),
                          BackdropFilter(
                            filter: ImageFilter.blur(sigmaY: 3, sigmaX: 3),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.2)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 14,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 50,
                            width: 170,
                            child: Row(
                              children: <Widget>[
                                Stack(children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        isLoveClicked = true;
                                        print(isLoveClicked.toString());
                                        if (isLoveClicked == true &&
                                            loveClickedCount == 0) {
                                          loveClickedCount = 1;
                                          loveBtn =
                                              "assets/icons/btn_loved_value.png";
                                          loveCount += 1;
                                          isLoved = loveCount.toString();
                                          print(isLoved);
                                        } else if (isLoveClicked == true &&
                                            loveClickedCount == 1) {
                                          loveClickedCount = 0;
                                          loveBtn =
                                              "assets/icons/btn_love_small.png";
                                          loveCount -= 1;
                                          isLoved = loveCount.toString();
                                          print('you dislike this event ' +
                                              isLoved);
                                        }
                                      });
                                    },
                                    child: SizedBox(
                                      height: loveBtn ==
                                              'assets/icons/btn_loved_value.png'
                                          ? 50
                                          : 50,
                                      width: loveBtn ==
                                              'assets/icons/btn_loved_value.png'
                                          ? 100
                                          : 50,
                                      child: Image.asset(loveBtn),
                                    ),
                                  ),
                                  Positioned(
                                    top: 16,
                                    left: 60,
                                    child: Text(isLoved,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold)),
                                  )
                                ]),
                                SizedBox(
                                  height: 50,
                                  width: 50,
                                  child:
                                      Image.asset('assets/icons/btn_chat.png'),
                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: phoneNumber == null || phoneNumber == ""
                                ? () {}
                                : () => launch("tel:" + phoneNumber.toString()),
                            child: SizedBox(
                              height: 50,
                              width: 50,
                              child: Image.asset(
                                  phoneNumber == null || phoneNumber == ""
                                      ? 'assets/icons/btn_phone.png'
                                      : 'assets/icons/btn_phone_active.png'),
                            ),
                          ),
                          GestureDetector(
                            onTap: email == null || email == ""
                                ? () {}
                                : () => launch("mailto:" + email.toString()),
                            child: SizedBox(
                              height: 50,
                              width: 50,
                              child: Image.asset(email == null || email == ""
                                  ? 'assets/icons/btn_mail.png'
                                  : 'assets/icons/btn_mail_active.png'),
                            ),
                          ),
                          GestureDetector(
                            onTap: website == null || website == ""
                                ? () {}
                                : () => launch(website.toString()),
                            child: SizedBox(
                              height: 50,
                              width: 50,
                              child: Image.asset(
                                  website == null || website == ""
                                      ? 'assets/icons/btn_web.png'
                                      : 'assets/icons/btn_web_active.png'),
                            ),
                          ),
                        ],
                      ),
                    ),
                    isPrivate == "0"
                        ? Container()
                        : Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Who\'s Invited'.toUpperCase(),
                                  style: TextStyle(color: eventajaGreenTeal),
                                ),
                                SizedBox(height: 10),
                                Container(
                                  height: 30,
                                  width: MediaQuery.of(context).size.width,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: goingData == null
                                        ? 0
                                        : goingData.length,
                                    itemBuilder: (BuildContext context, i) {
                                      return Padding(
                                        padding: EdgeInsets.only(right: 10),
                                        child: Container(
                                          constraints: BoxConstraints(
                                              maxHeight: 30,
                                              maxWidth: 30,
                                              minHeight: 30,
                                              minWidth: 30),
                                          height: 30,
                                          width: 30,
                                          decoration: BoxDecoration(
                                              boxShadow: <BoxShadow>[
                                                BoxShadow(
                                                    color: Colors.black,
                                                    blurRadius: 8,
                                                    offset: Offset(1.0, 1.0))
                                              ],
                                              shape: BoxShape.circle,
                                              image: DecorationImage(
                                                  image:
                                                      CachedNetworkImageProvider(
                                                          goingData[i]
                                                              ['photo']),
                                                  fit: BoxFit.fill)),
                                        ),
                                      );
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                    SizedBox(height: 10),
                    Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Who\'s Going'.toUpperCase(),
                            style: TextStyle(color: eventajaGreenTeal),
                          ),
                          SizedBox(height: 10),
                          Container(
                            height: 30,
                            width: MediaQuery.of(context).size.width,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount:
                                  goingData == null ? 0 : goingData.length,
                              itemBuilder: (BuildContext context, i) {
                                return Padding(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Container(
                                    constraints: BoxConstraints(
                                        maxHeight: 30,
                                        maxWidth: 30,
                                        minHeight: 30,
                                        minWidth: 30),
                                    height: 30,
                                    width: 30,
                                    decoration: BoxDecoration(
                                        boxShadow: <BoxShadow>[
                                          BoxShadow(
                                              color: Colors.black,
                                              blurRadius: 8,
                                              offset: Offset(1.0, 1.0))
                                        ],
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: CachedNetworkImageProvider(
                                                goingData[i]['photo']),
                                            fit: BoxFit.fill)),
                                  ),
                                );
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    DefaultTabController(
                      length: 3,
                      initialIndex: 0,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          TabBar(
                            unselectedLabelColor: Colors.grey,
                            tabs: <Widget>[
                              Tab(text: 'DETAILS'),
                              Tab(
                                text: 'ACTIVITY',
                              ),
                              Tab(
                                text: 'COMMENT',
                              )
                            ],
                          ),
                          Container(
                            color: Colors.grey.withOpacity(0.2),
                            padding: EdgeInsets.only(top: 10),
                            height: MediaQuery.of(context).size.height,
                            child: TabBarView(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20, right: 20, bottom: 13),
                                  child: ListView(
                                    children: <Widget>[
                                      Text('EVENT'),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        widget.eventDetailsData['name'] == null
                                            ? '-'
                                            : widget.eventDetailsData['name'],
                                        style: TextStyle(
                                            color: eventajaGreenTeal,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Divider(
                                        color: Colors.black26,
                                        height: 10,
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(widget
                                          .eventDetailsData['description']),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Divider(
                                        color: Colors.black26,
                                        height: 10,
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            height: 20,
                                            width: 20,
                                            child: Image.asset(
                                                'assets/icons/location-transparent.png'),
                                          ),
                                          Text('ADDRESS',
                                              style: TextStyle(
                                                  color: eventajaGreenTeal,
                                                  fontWeight: FontWeight.bold)),
                                        ],
                                      ),
                                      Text(address == null
                                          ? 'address'
                                          : address),
                                      showMap()
                                    ],
                                  ),
                                ),
                                Center(
                                  child: Text('ACTIVITY'),
                                ),
                                Text('this is comment section')
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 10,
                left: 26,
                child: isPrivate == '0'
                    ? Container()
                    : Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 20,
                            width: 10,
                            child: Image.asset('assets/icons/btn_gembok.png'),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text('Private Event',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12)),
                        ],
                      ),
              ),
              Positioned(
                top: 36,
                left: 26,
                child: Container(
                  width: 158,
                  height: 222,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      image: DecorationImage(
                          image:
                              NetworkImage(widget.eventDetailsData['picture']),
                          fit: BoxFit.fill)),
                ),
              ),
              Positioned(
                top: 36,
                left: 206,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 30,
                          width: 30,
                          child: Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: CachedNetworkImageProvider(
                                        creatorImageUri == null
                                            ? creatorImageUri.toString()
                                            : creatorImageUri.toString()))),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              creatorFullName == null
                                  ? 'loading'
                                  : creatorFullName.toString(),
                              style: TextStyle(
                                  fontSize: 12,
                                  color: eventajaGreenTeal,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                                creatorName == null
                                    ? 'loading'
                                    : creatorName.toString(),
                                style: TextStyle(color: Colors.white)),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'xx xx xx to xx xx xx',
                      style: TextStyle(color: eventajaGreenTeal),
                    ),
                    Container(
                      height: 30,
                      width: 200,
                      child: MarqueeWidget(
                        text: widget.eventDetailsData['name'].toUpperCase(),
                        textStyle: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        scrollAxis: Axis.horizontal,
                      ),
                    ),
                    SizedBox(height: 5),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                          height: 12,
                          child: Image.asset(
                              'assets/icons/location-transparent.png'),
                        ),
                        SizedBox(width: 5),
                        Container(
                          height: 16,
                          width: 200,
                          child: MarqueeWidget(
                            text: widget.eventDetailsData['address'],
                            scrollAxis: Axis.horizontal,
                            textStyle:
                                TextStyle(color: Colors.white, fontSize: 11),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                          height: 12,
                          child: Image.asset('assets/icons/btn_time_green.png'),
                        ),
                        SizedBox(width: 5),
                        Text(startTime.toString() + ' to ' + endTime.toString(),
                            style: TextStyle(fontSize: 11, color: Colors.white))
                      ],
                    )
                  ],
                ),
              ),
              Positioned(
                left: 205,
                top: 205,
                height: 50,
                width: 150,
                child: GestureDetector(
                  onTap: () {
                    if (ticketStat['salesStatus'] == null) {
                      return;
                    } else if (ticketType['type'] == 'free') {
                      showCupertinoDialog(
                          context: context,
                          builder: (BuildContext context) => SuccessPage());
                    } else if (ticketType['type'] == 'no_ticket') {
                      print('no ticket');
                      showDialog(
                        context: context,
                        child: new CupertinoAlertDialog(
                          title: new Text("Dialog Title"),
                          content: new Text("This is my content"),
                          actions: <Widget>[
                            CupertinoDialogAction(
                              isDefaultAction: true,
                              child: Text("Yes"),
                            ),
                            CupertinoDialogAction(
                              child: Text("No"),
                            )
                          ],
                        )
                      );
                    } else {
                      if (ticketStat['salesStatus'] == 'endSales' ||
                          ticketStat['availableTicketStatus'] == '0') {
                        return;
                      } else {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                SelectTicketWidget(
                                  eventID: detailData['id'],
                                  eventDate: detailData['dateStart'],
                                )));
                      }
                    }
                  },
                  child: Container(
                    height: 26,
                    width: 80,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        image: DecorationImage(
                          colorFilter: isTicketUnavailable == true
                              ? new ColorFilter.mode(
                                  Colors.black.withOpacity(0.4),
                                  BlendMode.dstATop)
                              : ColorFilter.mode(
                                  Colors.white, BlendMode.dstATop),
                          image: AssetImage(ticketTypeURI),
                          fit: BoxFit.fill,
                        )),
                    child: FractionallySizedBox(
                      alignment: Alignment.center,
                      heightFactor: .4,
                      child: Text(
                        ticketPrice,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              )
            ])
          ],
        ),
      ),
    );
  }

  Widget showMap() {
    StaticMapsProvider mapProvider = new StaticMapsProvider(
      GOOGLE_API_KEY: 'AIzaSyDjNpeyufzT81GAhQkCe85x83kxzfA7qbI',
      height: 200,
      width: MediaQuery.of(context).size.width.round(),
      latitude: lat,
      longitude: long,
    );

    String mapURI = mapProvider.toStringDeep();

    print(mapURI);

    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      child: mapProvider,
    );
  }

  testGetData() {
    print(widget.eventDetailsData['name']);
  }

  Future getInvitedUser() async {
    final invitedDataUrl = BaseApi().apiUrl +
        '/event/invited?X-API-KEY=${API_KEY}&event_id=${widget.eventDetailsData['id']}&page=all';
    final response = await http.get(invitedDataUrl, headers: {
      'Authorization': 'Basic YWRtaW46MTIzNA==',
      'cookie': session
    });

    print(response.statusCode);

    if (response.statusCode == 200) {
      setState(() {
        var extractedData = json.decode(response.body);
        invitedData = extractedData['data'];
        invitedUserList = invitedData['invited'];
      });

      print(invitedUserList);
    }
  }

  

  Future getEventDetailsSpecificInfo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      session = preferences.getString('Session');
    });

    final detailsInfoUrl = BaseApi().apiUrl +
        '/event/detail?X-API-KEY=${API_KEY}&eventID=4264'; //+
        //widget.eventDetailsData['id'];
    final response = await http.get(detailsInfoUrl, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print('event detail page -> ' + response.statusCode.toString());
    print('event detail page -> ' + response.body);

    if (response.statusCode == 200) {
      setState(() {
        var extractedData = json.decode(response.body);
        detailData = extractedData['data'];
        goingData = detailData['going']['data'];
        ticketType = extractedData['data']['ticket_type'];
        ticketStat = extractedData['data']['ticket'];
        creatorFullName = extractedData['data']['creatorFullName'];
        creatorName = extractedData['data']['creatorName'];
        creatorImageUri = extractedData['data']['creatorPhoto'];
        startTime = extractedData['data']['timeStart'];
        endTime = extractedData['data']['timeEnd'];
        isGoing = extractedData['data']['isGoing'];
        address = detailData['address'];
        lat = detailData['latitude'];
        long = detailData['longitude'];
        isPrivate = detailData['isPrivate'];
        isLoved = detailData['isLoved'];
        email = detailData['email'];
        phoneNumber = detailData['phone'];
        website = detailData['website'];
        eventID = detailData['id'];
        loveCount = int.parse(isLoved);

        if (detailData['status'] == 'active') {
          if (ticketType['isSetupTicket'].toString() == "1") {
            if (ticketStat['cheapestTicket'].toString() == "0") {
              setState(() {
                ticketPriceStringVisibility = false;
                ticketTypeURI = 'assets/btn_ticket/free-limited.png';
              });
            } else {
              ticketTypeURI = 'assets/btn_ticket/paid-value.png';
              ticketPrice = "Rp. " + ticketStat['cheapestTicket'].toString();
            }

            if (ticketStat['availableTicketStatus'].toString() == '0') {
              isTicketUnavailable = true;
            } else {
              isTicketUnavailable = false;
            }
          } else {
            if (ticketType['type'].toString() == 'free') {
              ticketTypeURI = "assets/btn_ticket/free";
            } else if (ticketType['type'].toString() == 'no_ticket') {
              ticketTypeURI = "assets/btn_ticket/no-ticket.png";
            } else if (ticketType['type'].toString() == 'on_the_spot') {
              ticketTypeURI = "assets/btn_ticket/ots.png";
            }
          }

          if (isGoing == "1" && ticketType['isSetupTicket'].toString() == "0") {
            ticketTypeURI = "assets/btn_ticket/going.png";
          }
        } else {
          ticketPrice = "";
          if (detailData['status'] == 'canceled') {
            ticketTypeURI = "assets/btn_ticket/canceled.png";
          } else if (detailData['status'] == 'ended') {
            ticketTypeURI = "assets/btn_ticket/event-ended.png";
          }
        }
      });
      preferences.setString('eventID', detailData['id']);
      print(detailData['id']);
      print(loveCount);
      print(isPrivate);
      print(creatorFullName);
      print(creatorName);
      print(creatorImageUri);
      print(startTime);
      print(endTime);
      print(ticketType['isSetupTicket']);
      print(ticketStat);
    }
  }
}
