import 'dart:convert';

import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class CategoryEventWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CategoryEventWidget();
  }
}

class _CategoryEventWidget extends State<CategoryEventWidget> {
  var session;
  List categoryEventData;
  List<Widget> mappedCategoryData;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchCategoryEvent();
  }

  @override
  Widget build(BuildContext context) {
    mappedCategoryData = categoryEventData?.map((categoryData) {
          return categoryData == null
              ? CircularProgressIndicator()
              : Builder(
                  builder: (BuildContext context) {
                    return SizedBox(
                      height: 100,
                      width: 115,
                      child: Container(
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(bottom: 20),
                            width: 65,
                            height: 65,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              boxShadow: <BoxShadow>[
                                                BoxShadow(
                                                    color: Colors.grey.withOpacity(0.5),
                                                    blurRadius: 3,
                                                    spreadRadius: 3,
                                                    offset: Offset(0.0, 0.0))
                                              ],
                              image: DecorationImage(
                                image: NetworkImage(categoryData['logo'],)
                              )
                            ),
                          ),
                          SizedBox(height: 8),
                          Text(categoryData['name'], overflow: TextOverflow.ellipsis, textAlign: TextAlign.center,)
                        ],
                      ),
                      )
                    );
                  },
                );
        })?.toList() ??
        [];

    // TODO: implement build
    return Container(
      height: 220,
      padding: EdgeInsets.symmetric(horizontal: 20),
      width: MediaQuery.of(context).size.width,
      child: categoryEventData == null ? Center(
                    child: Container(
                      width: 25,
                      height: 25,
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ) : ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Container(
            width: 1200,
            height: 220,
            child: Wrap(
                direction: Axis.horizontal,
                children: mappedCategoryData),
          )
        ],
      ),
    );
  }

  Future fetchCategoryEvent() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      session = preferences.getString('Session');
    });

    final categoryApi =
        BaseApi().apiUrl + '/category/list?X-API-KEY=${API_KEY}&page=1';
    final response = await http.get(categoryApi, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print(response.body);

    if (response.statusCode == 200) {
      var extractedData = json.decode(response.body);
      setState(() {
        categoryEventData = extractedData['data'];
      });
    }
  }
}
