import 'dart:convert';

import 'package:belajar_mvvm/Widgets/LatestEventWidget.dart';
import 'package:belajar_mvvm/Widgets/eventDetailsWidget.dart';
import 'package:belajar_mvvm/helper/Models/PopularEventModels.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:belajar_mvvm/helper/API/catalogModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'nearbyEventWidget.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'categoryEventWidget.dart';
import 'package:intl/date_symbol_data_local.dart';

//List<T> map<T>(List list, Function handler){
//  List<T> result = [];
//
//  for (var i = 0; i < list.length; i++){
//    result.add(handler(i, list[i]));
//  }
//
//  return result;
//}

class EventCatalog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _EventCatalogState();
  }
}

class _EventCatalogState extends State<EventCatalog> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  ///Variable untuk insialisasi Value awal
  List data;
  List bannerData;
  List discoverData;
  List popularPeopleData;
  List discoverPeopleData;
  List collectionData;
  List child;
  ListenPage geoPage = new ListenPage();
  List mediaData;

  String ticketPriceImageURI = 'assets/btn_ticket/paid-value.png';

  int _current = 0;

  ScrollController _scrollController = new ScrollController();
  List<Widget> mappedDataBanner;
  var session;

  ///Inisialisasi semua fungsi untuk fetching dan hal hal lain yang dibutuhkan
  @override
  void initState() {
    super.initState();
    fetchCatalog();
    fetchBanner();
    fetchDiscoverCatalog();
    fetchPopularPeople();
    fetchDiscoverPeople();
    fetchCollection();
    getMediaData();
    //WidgetsBinding.instance.addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    mappedDataBanner = bannerData?.map((bannerData) {
          return bannerData == null
              ? Center(child: CircularProgressIndicator())
              : Builder(
                  builder: (BuildContext context) {
                    return bannerData == null
                        ? Center(child: CircularProgressIndicator())
                        : Container(
                            width: MediaQuery.of(context).devicePixelRatio *
                                2645.0,
                            margin: EdgeInsets.only(
                                left: 6, right: 6, bottom: 15, top: 10),
                            decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(1.0, 1.0),
                                    blurRadius: 5,
                                  )
                                ],
                                borderRadius: BorderRadius.circular(15),
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: CachedNetworkImageProvider(
                                    bannerData["image"],
                                  ),
                                )),
                          );
                  },
                );
        })?.toList() ??
        [];

//    child = map<Widget>(
//        bannerData ?? [],
//        (index, i){
//          return Container(
//            width: MediaQuery.of(context).size.width,
//            margin: EdgeInsets.only(left: 6, right: 6, bottom: 15, top: 10),
//            decoration: BoxDecoration(
//                shape: BoxShape.rectangle,
//                boxShadow: <BoxShadow>[
//                  BoxShadow(
//                    color: Colors.black26,
//                    offset: Offset(1.0, 1.0),
//                    blurRadius: 5,
//                  )
//                ],
//                borderRadius: BorderRadius.circular(15),
//                image: DecorationImage(
//                  fit: BoxFit.cover,
//                  image: CachedNetworkImageProvider(
//                    i['image'],
//                  ),
//                )),
//          );
//        }
//    )?.toList() ?? [];

    return Column(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding:
                    EdgeInsets.only(top: 30, right: 25, left: 25, bottom: 30),
                child: Material(
                  borderRadius: BorderRadius.circular(40),
                  elevation: 5.0,
                  shadowColor: Colors.black,
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    autofocus: false,
                    autocorrect: false,
                    decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                          size: 30,
                        ),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                        hintText: 'Search event or people...',
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide:
                                BorderSide(color: Color.fromRGBO(0, 0, 0, 0))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(40),
                            borderSide:
                                BorderSide(color: Color.fromRGBO(0, 0, 0, 0)))),
                  ),
                )),
          ],
        ),
        DefaultTabController(
          length: 3,
          initialIndex: 0,
          child: Column(
            children: <Widget>[
              TabBar(
                labelColor: Colors.black,
                labelStyle: TextStyle(fontFamily: 'Proxima'),
                tabs: [
                  Tab(text: 'HOME'),
                  Tab(text: 'NEARBY'),
                  Tab(text: 'LATEST'),
                ],
                unselectedLabelColor: Colors.grey,
              ),
              Container(
                height: MediaQuery.of(context).size.height / 1.5,
                child: RefreshIndicator(
                  key: _refreshIndicatorKey,
                  onRefresh: _refresh,
                  child: TabBarView(
                    children: [
                      ListView(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Stack(
                              children: <Widget>[
                                mappedDataBanner == null
                                    ? Center(
                                        child: CircularProgressIndicator(),
                                      )
                                    : banner(),
//                                Positioned(
//                                  top: 150,
//                                  left: 0,
//                                  right: 0,
//                                  child: Row(
//                                    mainAxisAlignment: MainAxisAlignment.center,
//                                    children: map<Widget>(mappedDataBanner, (index, url){
//                                      return Container(
//                                        width: 10.0,
//                                        height: 10.0,
//                                        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
//                                        decoration: BoxDecoration(
//                                          shape: BoxShape.circle,
//                                          color: _current == index ? Colors.white : Colors.grey
//                                        ),
//                                      );
//                                    })
//                                  ),
//                                )
                              ],
                            ),
                          ),
                          CategoryEventWidget(),
                          collection(),
                          collectionImage(),
                          Padding(
                            padding:
                                EdgeInsets.only(left: 25, top: 15, bottom: 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text('Popular Event',
                                        style: TextStyle(
                                            fontFamily: 'Proxima',
                                            fontSize: 28,
                                            fontWeight: FontWeight.bold)),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                      horizontal:
                                          MediaQuery.of(context).size.width /
                                              7.5,
                                    )),
                                    GestureDetector(
                                      onTap: () {},
                                      child: Text('See All >',
                                          style: TextStyle(
                                              color: eventajaGreenTeal,
                                              fontFamily: 'Proxima')),
                                    ),
                                  ],
                                ),
                                Text('Find the most popular event', style: TextStyle(color: Color(0xFF868686))),
                              ],
                            ),
                          ),
                          popularEventContent(),
                          SizedBox(height: 15),
                          mediaHeader(),
                          SizedBox(height: 7),
                          mediaContent(),
                          SizedBox(height: 15),
                          popularPeople(),
                          popularPeopleImage(),
                          SizedBox(height: 15),
                          discoverEvent(),
                          discoverEventContent(),
                          SizedBox(height: 15),
                          discoverPeople(),
                          discoverPeopleImage()
                        ],
                      ),
                      Container(
                        child: Center(
                          child: ListenPage(),
                        ),
                      ),
                      Container(
                        child: LatestEventWidget(),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Future<Null> _refresh() {
    fetchCollection();
    fetchPopularPeople();
    fetchBanner();
    fetchCatalog();
    fetchDiscoverCatalog();
    fetchDiscoverPeople();
  }

  Widget popularEventContent() {
    return Container(
        height: 354,
        child: isLoading
            ? Center(child: CircularProgressIndicator())
            : new ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, i) {
                  return new Container(
                      width: i == 0 ? 200 : 190,
                      child: Padding(
                        padding: i == 0
                            ? EdgeInsets.only(left: 25)
                            : EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            EventDetailsConstructView(
                                              eventDetailsData: data[i],
                                            )));
                              },
                              child: Container(
                                height: 250,
                                width: 190,
                                child: CachedNetworkImage(
                                  imageUrl: data[i]['picture'],
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) => Container(
                                          child: Image.asset(
                                        'assets/grey-fade.jpg',
                                        fit: BoxFit.fill,
                                      )),
                                ),
                              ),
                            ),
                            Text(data[i]["dateStart"],
                                style: TextStyle(color: eventajaGreenTeal),
                                textAlign: TextAlign.start),
                            Text(
                              data[i]["name"],
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            Text(data[i]["address"],
                                overflow: TextOverflow.ellipsis),
                            Container(
                              height: 40,
                              width: 150,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(ticketPriceImageURI),
                                      fit: BoxFit.fill)),
                              child: Center(child: Text('harga')),
                            )
                          ],
                        ),
                      ));
                }));
  }

  ///Construct DiscoverEvent Widget

  Widget discoverEvent() {
    return new Padding(
      padding: EdgeInsets.only(left: 25, top: 15, bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text('Discover Event',
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
              Padding(
                  padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 9.7,
              )),
              GestureDetector(
                onTap: () {},
                child: Text(
                  'See All >',
                  style: TextStyle(color: eventajaGreenTeal),
                ),
              ),
            ],
          ),
          Text('Discover the undiscovered', style: TextStyle(color: Color(0xFF868686))),
        ],
      ),
    );
  }

  Widget discoverEventContent() {
    return Container(
        height: 310,
        child: isLoading
            ? Center(child: CircularProgressIndicator())
            : new ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: discoverData == null ? 0 : discoverData.length,
                itemBuilder: (BuildContext context, i) {
                  return new Container(
                      width: i == 0 ? 200 : 190,
                      child: Padding(
                        padding: i == 0
                            ? EdgeInsets.only(left: 25)
                            : EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: 250,
                              width: 190,
                              child: CachedNetworkImage(
                                fit: BoxFit.fill,
                                imageUrl: discoverData[i]["picture"],
                                placeholder: (context, url) => new Container(
                                      child: Image.asset(
                                        'assets/grey-fade.jpg',
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                              ),
                            ),
                            Text(discoverData[i]["dateStart"],
                                style: TextStyle(color: eventajaGreenTeal)),
                            Text(discoverData[i]["name"],
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                                overflow: TextOverflow.ellipsis),
                            Text(discoverData[i]["address"],
                                overflow: TextOverflow.ellipsis),
                          ],
                        ),
                      ));
                }));
  }

  ///Construct PopularPeople Widget

  Widget popularPeople() {
    return Padding(
      padding: EdgeInsets.only(left: 25, top: 15, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text('Popular People',
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
              Padding(
                  padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 9.75,
              )),
              GestureDetector(
                onTap: () {},
                child: Text(
                  'See All >',
                  style: TextStyle(color: eventajaGreenTeal),
                ),
              ),
            ],
          ),
          Text('Find the most popular people', style: TextStyle(color: Color(0xFF868686))),
          SizedBox(height: 15),
        ],
      ),
    );
  }

  Widget popularPeopleImage() {
    return Container(
      height: 80,
      child: isLoading
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount:
                  popularPeopleData == null ? 0 : popularPeopleData.length,
              itemBuilder: (BuildContext context, i) {
                return new Container(
                  width: i == 0 ? 85 : 70,
                  padding: i == 0
                      ? EdgeInsets.only(left: 25)
                      : EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 60,
                        decoration: BoxDecoration(
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(1.0, 1.0),
                                  blurRadius: 3)
                            ],
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: CachedNetworkImageProvider(
                                  popularPeopleData[i]["photo"]),
                              fit: BoxFit.fill,
                            )),
                      ),
                    ],
                  ),
                );
              },
            ),
    );
  }

  Widget collection() {
    return Padding(
      padding: EdgeInsets.only(left: 25, top: 15, bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text('Collection',
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
              Padding(
                  padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 5,
              )),
            ],
          ),
          SizedBox(
            height: 6
          ),
          Text('Check out our hand-picked collectoins bellow', style: TextStyle(color: Color(0xFF868686))),
        ],
      ),
    );
  }

  Widget collectionImage() {
    return Container(
      height: 90,
      child: isLoading
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: collectionData == null ? 0 : collectionData.length,
              itemBuilder: (BuildContext context, i) {
                return new Container(
                  width: 150,
                  margin: i == 0
                      ? EdgeInsets.only(left: 25)
                      : EdgeInsets.only(left: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 70,
                        width: 150,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: CachedNetworkImage(
                            imageUrl: collectionData[i]['image'],
                            placeholder: (context, url) => Container(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: Image.asset(
                                      'assets/grey-fade.jpg',
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
    );
  }

  ///Construct DiscoverPeople Widget

  Widget discoverPeople() {
    return Padding(
      padding: EdgeInsets.only(left: 25, top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text('Discover People',
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
              Padding(
                  padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 9.5,
              )),
              GestureDetector(
                onTap: () {},
                child: Text(
                  'See All >',
                  style: TextStyle(color: eventajaGreenTeal),
                ),
              ),
            ],
          ),
          Text('Find more people to follow', style: TextStyle(color: Color(0xFF868686))),
          SizedBox(height: 15),
        ],
      ),
    );
  }

  Widget discoverPeopleImage() {
    return Container(
      height: 80,
      child: isLoading
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount:
                  discoverPeopleData == null ? 0 : discoverPeopleData.length,
              itemBuilder: (BuildContext context, i) {
                return new Container(
                  width: i == 0 ? 85 : 70,
                  padding: i == 0
                      ? EdgeInsets.only(left: 25)
                      : EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 60,
                        decoration: BoxDecoration(
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(1.0, 1.0),
                                  blurRadius: 3)
                            ],
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: CachedNetworkImageProvider(
                                  discoverPeopleData[i]["photo"]),
                              fit: BoxFit.fill,
                            )),
                      ),
                    ],
                  ),
                );
              },
            ),
    );
  }

  ///Construct BannerCarousel Widget
  ///
  Widget banner() {
    return CarouselSlider(
      height: 200,
      items: bannerData == null
          ? [
              Center(
                child: CircularProgressIndicator(),
              )
            ]
          : mappedDataBanner,
      enlargeCenterPage: false,
      initialPage: 0,
      autoPlay: true,
      aspectRatio: 2.0,
      viewportFraction: 1.0,
      onPageChanged: (index) {
        setState(() {
          _current = index;
        });
      },
    );
  }

  Widget bannerCarousel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: 180,
          width: 350,
          child: new Carousel(
            boxFit: BoxFit.cover,
            images: mappedDataBanner,
            dotSize: 10,
            dotSpacing: 15.0,
            dotColor: Colors.white,
            dotBgColor: Color.fromRGBO(0, 0, 0, 0),
            borderRadius: true,
            radius: Radius.circular(15),
          ),
        ),
      ],
    );
  }

  Widget mediaHeader() {
    return new Padding(
      padding: EdgeInsets.only(left: 25, top: 15, bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Row(
                children: <Widget> [
                  Container(
                    width: 27,
                    height: 27,
                    child: Image.asset('assets/logo_eventevent.png'),
                  ),
                  SizedBox(width: 5),
                  Text('Media',
                  style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
                ]
              ),
              Padding(
                  padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 4.5,
              )),
              GestureDetector(
                onTap: () {},
                child: Text(
                  'See All >',
                  style: TextStyle(color: eventajaGreenTeal),
                ),
              ),
            ],
          ),
          Text('-'),
        ],
      ),
    );
  }

  Widget mediaContent() {
    return Container(
        height: 230,
        alignment: Alignment.centerLeft,
        child: isLoading
            ? Center(
                child: CircularProgressIndicator(
                backgroundColor: Colors.white,
              ))
            : new ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: mediaData == null ? 0 : mediaData.length,
                itemBuilder: (BuildContext context, i) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 10, left: 10),
                    child: new Container(
                      alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(15)
                        ),
                        width: i == 0 ? 250 : 250,
                        child: Padding(
                          padding: i == 0
                              ? EdgeInsets.only(left: 0)
                              : EdgeInsets.only(left: 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) =>
                                  //             EventDetailsConstructView(eventDetailsData: data[i],)));
                                },
                                child: Container(
                                  height: 120,
                                  width: i == 0 ? 250 : 250,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                                    image: DecorationImage(
                                      image: NetworkImage(
                                        mediaData[i]['banner'],
                                      ),
                                      fit: BoxFit.fill
                                    )
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 9
                              ),
                              // Text(data[i]["dateStart"],
                              //     style: TextStyle(color: eventajaGreenTeal),
                              //     textAlign: TextAlign.start),
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: 220,
                                      child: Text(
                                        'asdfghhtij aijitaj sjakj \n ofkoakf ffffa gggdssef ffwf',
                                        //mediaData[i]["title"],
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontSize: 20, fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.start,
                                            maxLines: 2,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 9),
                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      height: 30,
                                      width: 30,
                                      decoration:
                                          BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                              image: NetworkImage(mediaData[i]['creator']['photo']),
                                              fit: BoxFit.fill
                                            )
                                          ),
                                    ),
                                    SizedBox(width: 10,),
                                    Text(mediaData[i]['creator']['fullName'] == null
                                        ? '-'
                                        : mediaData[i]['creator']['fullName'])
                                  ],
                                ),
                              ),
                              // Container(
                              //   height: 40,
                              //   width: 150,
                              //   decoration: BoxDecoration(
                              //     image: DecorationImage(
                              //         image: AssetImage(ticketPriceImageURI),
                              //       fit: BoxFit.fill
                              //     )
                              //   ),
                              //   child: Center(child: Text('harga')),
                              // )
                            ],
                          ),
                        )),
                  );
                }));
  }

  List<Catalog> _list = [];
  var isLoading = false;

  Future getMediaData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String session;
    setState(() {
      session = preferences.getString('Session');
    });

    String url =
        BaseApi().restUrl + '/media?X-API-KEY=${API_KEY}&search=&sort=&sort_by';
    final response =
        await http.get(url, headers: {'Authorization': AUTHORIZATION_KEY});

    print(response.body);

    if (response.statusCode == 200) {
      var extractedData = json.decode(response.body);
      mediaData = extractedData['data'];

      print(mediaData);
    }
  }

  ///Untuk Fetching Gambar banner
  Future fetchBanner() async {
    setState(() {
      isLoading = true;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      session = preferences.getString('Session');
    });

    final bannerApiUrl =
        'http://home.eventeventapp.com/api/banner/timeline?X-API-KEY=47d32cb10889cbde94e5f5f28ab461e52890034b';
    final response = await http.get(bannerApiUrl, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print(
        'event catalog widget - fetchBanner' + response.statusCode.toString());

    if (response.statusCode == 200) {
      setState(() {
        var extractedData = json.decode(response.body);
        bannerData = extractedData['data'];

        isLoading = false;
      });
    } else if (response.statusCode == 403) {
      setState(() {
        isLoading = false;
      });

      Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 3),
        content: Text('You are not authorized!',
            style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red,
      ));
    }
  }

  Future fetchCollection() async {
    setState(() {
      isLoading = true;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      session = preferences.getString('Session');
    });

    final collectionUrl =
        'https://home.eventeventapp.com/api/collections/list?X-API-KEY=47d32cb10889cbde94e5f5f28ab461e52890034b&page=1';
    final response = await http.get(collectionUrl, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print('eventCatalogWidget - fetch collection ' +
        response.statusCode.toString());

    if (response.statusCode == 200) {
      print('fetched collection data');
      setState(() {
        var extractedData = json.decode(response.body);
        collectionData = extractedData['data'];

        isLoading = false;
      });
    } else if (response.statusCode == 403) {
      setState(() {
        isLoading = false;
      });
      Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 3),
        content: Text('You are not authorized!',
            style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red,
      ));
    }
  }

  ///Untuk Fetching gambar PopularPeople
  Future fetchPopularPeople() async {
    setState(() {
      isLoading = true;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      session = preferences.getString('Session');
    });

    final popularPeopleUrl =
        'https://home.eventeventapp.com/api/user/popular?X-API-KEY=47d32cb10889cbde94e5f5f28ab461e52890034b&page=1&total=20';
    final response = await http.get(popularPeopleUrl, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print('eventCatalogWidget - fetch pop people' +
        response.statusCode.toString());

    if (response.statusCode == 200) {
      print('fetched data');
      setState(() {
        var extractedData = json.decode(response.body);
        popularPeopleData = extractedData['data'];

        isLoading = false;
      });
    } else if (response.statusCode == 403) {
      setState(() {
        isLoading = false;
      });
      Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 3),
        content: Text('You are not authorized!',
            style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red,
      ));
    }
  }

  ///Untuk fetching gambar DiscoverPeople
  Future fetchDiscoverPeople() async {
    setState(() {
      isLoading = true;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      session = preferences.getString('Session');
    });

    final popularPeopleUrl =
        'https://home.eventeventapp.com/api/user/discover?X-API-KEY=47d32cb10889cbde94e5f5f28ab461e52890034b&page=1&total=20';
    final response = await http.get(popularPeopleUrl, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print('eventCatalogWidget - fetch discover people ' +
        response.statusCode.toString());

    if (response.statusCode == 200) {
      print('fetched data discoverPeople');
      setState(() {
        var extractedData = json.decode(response.body);
        discoverPeopleData = extractedData['data'];

        isLoading = false;
      });
    } else if (response.statusCode == 403) {
      setState(() {
        isLoading = false;
      });
      Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 3),
        content: Text('You are not authorized!',
            style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red,
      ));
    }
  }

  ///Untuk fetching segala macem yang ada di DiscoverCatalog

  Future fetchDiscoverCatalog() async {
    setState(() {
      isLoading = true;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      session = preferences.getString('Session');
    });

    final discoverApiUrl =
        'https://home.eventeventapp.com/api/event/discover?X-API-KEY=47d32cb10889cbde94e5f5f28ab461e52890034b&page=1&total=20';
    final response = await http.get(discoverApiUrl, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print('eventCatalogWidget - discover widget ' +
        response.statusCode.toString());

    if (response.statusCode == 200) {
      setState(() {
        var extractedData = json.decode(response.body);
        discoverData = extractedData['data'];

        isLoading = false;
      });
    } else if (response.statusCode == 403) {
      setState(() {
        isLoading = false;
      });
      Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 3),
        content: Text('You are not authorized!',
            style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red,
      ));
    }
  }

  ///Untuk fetching segala macem yang ada di PopularEvent / Catalog
  Future fetchCatalog() async {
    setState(() {
      isLoading = true;
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      session = preferences.getString('Session');
    });

    final catalogApiUrl = BaseApi().apiUrl +
        '/event/popular?X-API-KEY=47d32cb10889cbde94e5f5f28ab461e52890034b&page=1&total=20';
    final response = await http.get(catalogApiUrl, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print('eventCatalogWidget' + response.statusCode.toString());

    if (response.statusCode == 200) {
      setState(() {
        var extractedData = json.decode(response.body);
        data = extractedData['data'];

        isLoading = false;
      });
    } else if (response.statusCode == 403) {
      setState(() {
        isLoading = false;
      });
      Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 3),
        content: Text('You are not authorized!',
            style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red,
      ));
    }
  }
}
