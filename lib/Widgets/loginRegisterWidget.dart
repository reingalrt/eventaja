import 'dart:convert';

import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:package_info/package_info.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'RegisterFacebook.dart';
import 'dashboardWidget.dart';

class LoginRegisterWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginRegisterWidget();
  }
}

class _LoginRegisterWidget extends State<LoginRegisterWidget> {
  void initiateFacebookLogin() async {
    var facebookLogin = FacebookLogin();
    var facebookLoginResult = await facebookLogin
        .logInWithReadPermissions(['email', 'public_profile', 'user_friends', 'user_gender']);

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.loggedIn:
        print(facebookLoginResult.accessToken.token);
        goLoginFb(facebookLoginResult.accessToken.token);
        onLoginStatusChanged(true);
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        onLoginStatusChanged(false);
        break;
      case FacebookLoginStatus.error:
        print("Error");
        onLoginStatusChanged(false);
        break;
    }
  }

  Future goLoginFb(String fbToken) async {
    String url =
        BaseApi().apiUrl + '/signin/facebook?X-API-KEY=$API_KEY&token=$fbToken';
    final response =
        await http.get(url, headers: {'Authorization': AUTHORIZATION_KEY});

    print(response.statusCode);
    print(response.body);
    print(response.headers);

    if (response.statusCode == 200) {
      print('loginSuccess');
      
      SharedPreferences preferences = await SharedPreferences.getInstance();

      preferences.setString('Session', response.headers['set-cookie']);
      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => DashboardWidget()));
    } else {
      var extractedData = json.decode(response.body);
      String message = extractedData['desc'];

      if (message == "User is not register") {
        var graphResponse = await http.get(
            'https://graph.facebook.com/v3.3/me?fields=name,first_name,last_name,email,picture,birthday,gender,accounts{phone}&access_token=$fbToken');
        var graphData = json.decode(graphResponse.body);
        String id = null;
        id = graphData['id'];
        SharedPreferences prefs = await SharedPreferences.getInstance();
        setState(() {
          prefs.setString('REGIS_FB_ID', id);
          prefs.setString('REGIS_FB_PHOTO',
              "https://graph.facebook.com/" + id + "/picture?type=large");
          prefs.setString('REGIS_FB_BIRTH_DATE', graphData['birthday']);
          prefs.setString('REGIS_FB_GENDER', graphData['gender']);
          prefs.setString('REGIS_FB_EMAIL', graphData['email']);
          prefs.setString('REGIS_FB_FULLNAME', graphData['name']);
          prefs.setString('REGIS_FB_FIRST_NAME', graphData['first_name']);
          prefs.setString('REGIS_FB_LAST_NAME', graphData['last_name']);
        });
        print(graphData);

        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => RegisterFacebook()));
      }
    }
  }

  bool isLoggedIn = false;

  void onLoginStatusChanged(bool isLoggedIn) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
    });
  }

  void getPackageInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;

    print(appName);
    print(packageName);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPackageInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 100),
            ),
            FractionallySizedBox(
              widthFactor: .5,
              child: Image.network(
                  'http://icons.iconarchive.com/icons/mattahan/umicons/256/Letter-E-icon.png'),
            ),
            Padding(
              padding: EdgeInsets.only(top: 100),
            ),
            Center(
              child: Text('DAFTAR DAN MULAI',
                  style: TextStyle(fontSize: 10.0, color: Colors.grey)),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ButtonTheme(
                    minWidth: 150,
                    child: RaisedButton(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                      color: eventajaGreenTeal,
                      onPressed: () {
                        Navigator.of(context).pushNamed('/Login');
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 3),
                  ),
                  ButtonTheme(
                    minWidth: 150.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      child: Text(
                        'Register',
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                      color: eventajaGreenTeal,
                      onPressed: () {
                        Navigator.pushNamed(context, '/Register');
                      },
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.black,
              height: 10,
              indent: 5,
            ),
            ButtonTheme(
              minWidth: 300,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                color: Colors.blueAccent,
                child: Text(
                  'MASUK DENGAN FACEBOOK',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
                onPressed: () {
                  initiateFacebookLogin();
                },
              ),
            ),
            ButtonTheme(
              minWidth: 300,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                color: Colors.white,
                child: Text(
                  'MASUK DENGAN GOOGLE',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, color: Colors.black38),
                ),
                onPressed: () {},
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 15, left: 5, right: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('dengan mendaftar, anda setuju dengan',
                      style: TextStyle(fontSize: 8)),
                  GestureDetector(
                    child: Text(
                      ' Syarat',
                      style:
                          TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(' dan', style: TextStyle(fontSize: 8)),
                  GestureDetector(
                    child: Text(
                      ' Kebijakan Pribadi',
                      style:
                          TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
