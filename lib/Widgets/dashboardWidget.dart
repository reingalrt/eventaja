import 'package:belajar_mvvm/Widgets/loginRegisterWidget.dart';
import 'package:belajar_mvvm/Widgets/profileWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'eventCatalogWidget.dart';

class DashboardWidget extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DashboardWidgetState();
  }
}

class _DashboardWidgetState extends State<DashboardWidget> {
  int _selectedPage = 0;
  final _pageOptions = [
    EventCatalog(),
    Container(),
    Container(),
    Container(),
    ProfileWidget(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: SafeArea(
        bottom: true,
        child: CupertinoTabBar(
                  currentIndex: _selectedPage,
                    onTap: (int index){
                      setState(() {
                        _selectedPage = index;
                      });
                    },
                    items: <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                    title: Text('DISCOVER', style: TextStyle(color: Colors.black26),),
                    icon: Image.asset("assets/bottom-bar/home-white.png", height: 25, width: 25),
                    activeIcon: Image.asset("assets/bottom-bar/home-green.png", height: 25, width: 25,)
                  ),
                  BottomNavigationBarItem(
                    title: Text('TIMELINE', style: TextStyle(color: Colors.black26),),
                    icon: Image.asset("assets/bottom-bar/timeline-white.png", height: 25, width: 25,),
                    activeIcon: Image.asset("assets/bottom-bar/timeline-green.png", height: 25, width: 25)
                  ),
                  BottomNavigationBarItem(
                    title: Text('POST', style: TextStyle(color: Colors.black26),),
                    icon: Image.asset("assets/bottom-bar/new-something-white.png", height: 25, width: 25),
                    activeIcon: Image.asset("assets/bottom-bar/new-something-green.png", height: 25, width: 25)
                  ),
                  BottomNavigationBarItem(
                    title: Text('NOTIFICATION', style: TextStyle(color: Colors.black26),),
                    icon: Image.asset("assets/bottom-bar/notification-white.png", height: 25, width: 25),
                    activeIcon: Image.asset("assets/bottom-bar/notification-green.png", height: 25, width: 25),
                  ),
                  BottomNavigationBarItem(
                    title: Text('PROFILE', style: TextStyle(color: Colors.black26),),
                    icon: Image.asset("assets/bottom-bar/user-white.png", height: 25, width: 25),
                    activeIcon: Image.asset("assets/bottom-bar/user-green.png", height: 25, width: 25),
                  )
                ]),
      ),
      body: _pageOptions[_selectedPage],
    );
  }
}
