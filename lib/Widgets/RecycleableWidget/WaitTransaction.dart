import 'dart:async';
import 'dart:convert';
import 'package:quiver/async.dart';

import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart';
import 'package:belajar_mvvm/helper/inappbrowser/chromeSafariBrowser.dart';

class WaitTransaction extends StatefulWidget {
  final expDate;
  final String transactionID;
  final String finalPrice;

  const WaitTransaction({Key key, this.expDate, this.transactionID, this.finalPrice})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _WaitTransactionState();
    ;
  }
}

// InitInAppBrowser inAppBrowserFallback = new InitInAppBrowser();

class _WaitTransactionState extends State<WaitTransaction> {
  String month;
  String hour;
  String min;
  String sec;
  CountdownTimer timer;
  String bank_code;
  String bank_acc;
  String bank_number;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  
  // MyChromeSafariBrowser myChromeSafariBrowser = new MyChromeSafariBrowser(inAppBrowserFallback);

  DateTime dateTime;

  var expDate;

  Map<String, dynamic> paymentData;

  Timer countdownTimer;

  void startCounter(String expired) {
    int hoursStart, minuteStart, secondStart;
    String strHour, strMinute, strSecond;

    dateTime = DateTime.parse(expired);

    hoursStart = dateTime.hour;
    minuteStart = dateTime.minute;
    secondStart = dateTime.second;

    setState(() {
      hour = hoursStart.toString();
      min = minuteStart.toString();
      sec = secondStart.toString();
    });

    print(hoursStart.toString() +
        minuteStart.toString() +
        secondStart.toString());

    // timer = new CountdownTimer();
  }

  Future getBankInfo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String bankNumber;
    String bankCode;
    String bankAcc;

    bankNumber = preferences.getString('bank_acc');
    bankCode = preferences.getString('bank_code');
    bankAcc = preferences.getString('bank_name');

    setState(() {
      bank_number = bankNumber;
      bank_code = bankCode;
      bank_acc = bankAcc;
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getTransactionDetail();
    getBankInfo();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white.withOpacity(0.9),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.close,
            size: 35,
            color: eventajaGreenTeal,
          ),
        ),
      ),
      body: paymentData == null
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 380,
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 200,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: eventajaGreenTeal),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Text('Complete Payment In',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold)),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('${hour}',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  ':',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  '$min',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  ':',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  '$sec',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('H',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20)),
                                SizedBox(
                                  width: 35,
                                ),
                                Text('M',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20)),
                                SizedBox(
                                  width: 35,
                                ),
                                Text('S',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20)),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Complete payment before ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text('${widget.expDate}',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold))
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'TRANSFER AMOUNT',
                            style:
                                TextStyle(fontSize: 20, color: Colors.black45),
                          ),
                          SizedBox(height: 15),
                          Text(
                            'Rp. ' +
                                widget.finalPrice.toString(),
                            style: TextStyle(
                                fontSize: 50, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 15),
                          Text(
                            'Eventevent will automatically check your payment. It may take up to 1 hour to process',
                            style: TextStyle(color: Colors.grey, fontSize: 12),
                            textAlign: TextAlign.center,
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'TRANSFER TO',
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(height: 15),
                GestureDetector(
                  onTap: (){
                    if(paymentData['payment_method_id'] == '2'){
                      print('string copied');
                      Clipboard.setData(ClipboardData(text: bank_number));
                      print(Clipboard.getData('text/plain'));
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text('Text Coppied!'),
                      ));
                    }
                    else if(paymentData['payment_method_id'] == '9'){
                      String url = paymentData['payment']['data_vendor']['payment_url'];
                      launch(url, forceSafariVC: true, enableJavaScript: true, forceWebView: true, statusBarBrightness: Brightness.light);
                    }
                  },
                  child: Container(
                    height: 130,
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.only(
                        left: 15, right: 7, top: 10, bottom: 10),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 1,
                              offset: Offset(1, 1))
                        ],
                        borderRadius: BorderRadius.circular(15)),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: SizedBox(
                            height: 30,
                            child: Image.asset(bank_code ==
                                    'BNI'
                                ? 'assets/drawable/bni.png'
                                : 'assets/drawable/bri.png'),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                bank_acc,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.black54),
                              ),
                              SizedBox(height: 10),
                              Text(
                                  bank_code,
                                  style: TextStyle(color: Colors.grey)),
                              SizedBox(height: 10),
                              Text(
                                bank_number,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    color: Colors.black54),
                              ),
                            ])
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Future getTransactionDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var session;

    setState(() {
      session = prefs.getString('Session');
    });

    String url = BaseApi().apiUrl +
        '/ticket_transaction/detail?transID=${widget.transactionID}&X-API-KEY=${API_KEY}';
    final response = await http.get(url,
        headers: {'Authorization': AUTHORIZATION_KEY, 'cookie': session});

    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      var extractedData = json.decode(response.body);
      setState(() {
        paymentData = extractedData['data'];
        startCounter(paymentData['expired_time']);
        print(paymentData);
      });
    }
  }

  String get timerString {
    Duration duration = new Duration(hours: 60, minutes: 10, seconds: 5);
    return '${duration.inHours}:${duration.inMinutes % 60}:${duration.inSeconds % 60}'
        .toString()
        .padLeft(2, '0');
  }
}
