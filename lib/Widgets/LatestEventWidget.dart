import 'dart:convert';

import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LatestEventWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LatestEventWidget();
  }
}

class _LatestEventWidget extends State<LatestEventWidget> {

  var session;
  List latestEventData;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchLatestEvent();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Container(
            child: latestEventData == null
                ? Center(
                    child: Container(
                      width: 25,
                      height: 25,
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  )
                : ListView.builder(
                    itemCount:
                        latestEventData == null ? 0 : latestEventData.length,
                    itemBuilder: (BuildContext, i) {
                      return new Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        height: 180,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey,
                                offset: Offset(1.0, 1.0),
                                blurRadius: 5)
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(10),
                              height: 180,
                              width: 120,
                              child: CachedNetworkImage(
                                fit: BoxFit.fill,
                                imageUrl: latestEventData[i]['picture'],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        latestEventData[i]['dateStart'],
                                        style: TextStyle(
                                            color: eventajaGreenTeal,
                                            fontSize: 15),
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                5,
                                      ),
                                      Text('Jarak Km')
                                    ],
                                  ),
                                  Text(
                                    latestEventData[i]['name'],
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 15,
                                        width: 15,
                                        child: Image.asset('assets/icons/location-transparent.png'),
                                      ),
                                      SizedBox(width: 2,),
                                      Expanded(child: Text(latestEventData[i]['address'], overflow: TextOverflow.ellipsis,))
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  )));
  }

  Future fetchLatestEvent() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      session = preferences.getString('Session');
    });

    final latestEventApi = BaseApi().apiUrl + '/event/list?page=1&X-API-KEY=${API_KEY}';
    final response = await http.get(latestEventApi, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print(response.body);

    if(response.statusCode == 200){
      setState(() {
        var extractedData = json.decode(response.body);
        latestEventData = extractedData['data'];
      });
    }
  }
}
