import 'dart:async';
import 'dart:convert';

import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:auto_size_text/auto_size_text.dart';

var session;
List nearbyEventData;

class ListenPage extends StatefulWidget {
  @override
  _ListenPageState createState() => _ListenPageState();
}

class _ListenPageState extends State<ListenPage> {
  Location location = new Location();

  Map<String, double> currentLocation = new Map();
  StreamSubscription<Map<String, double>> locationSubcription;
  String err;
  var adresses;
  var fullAddr;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchCurrentLocationEvent(1);
    //getLocationName();

    initPlatformState();
    locationSubcription =
        location.onLocationChanged().listen((Map<String, double> result) {
      setState(() {
        currentLocation = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: Colors.black12,
            child: nearbyEventData == null
                ? Center(
                    child: Container(
                      width: 25,
                      height: 25,
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  )
                : ListView.builder(
                    itemCount:
                        nearbyEventData == null ? 0 : nearbyEventData.length,
                    itemBuilder: (BuildContext, i) {
                      return new Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        height: 180,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey,
                                offset: Offset(1.0, 1.0),
                                blurRadius: 5)
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(10),
                              height: 180,
                              width: 120,
                              child: CachedNetworkImage(
                                fit: BoxFit.fill,
                                imageUrl: nearbyEventData[i]['picture'],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        nearbyEventData[i]['dateStart'],
                                        style: TextStyle(
                                            color: eventajaGreenTeal,
                                            fontSize: 15),
                                      ),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                5,
                                      ),
                                      Text('Jarak Km')
                                    ],
                                  ),
                                  Text(
                                    nearbyEventData[i]['name'],
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 15,
                                        width: 15,
                                        child: Image.asset(
                                            'assets/icons/location-transparent.png'),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Expanded(
                                          child: Text(
                                        nearbyEventData[i]['address'],
                                        overflow: TextOverflow.ellipsis,
                                      ))
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  )));
  }

  void initPlatformState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, double> myLocation;
    try {
      myLocation = await location.getLocation();
      err = "";
    } on PlatformException catch (e) {
      if (e.code == "PERMISSION_DENIED") {
        err = 'Permission Denied';
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        err =
            'Permission denied - please ask the user to enable location service';
      }
      myLocation = null;
    }
    setState(() {
      currentLocation = myLocation;
      if (currentLocation['latitude'] != null &&
          currentLocation['longitude'] != null) {
        prefs.setString('latitude', currentLocation['latitude'].toString());
        prefs.setString('longitude', currentLocation['longitude'].toString());
      }
    });
  }

  Future fetchCurrentLocationEvent(int page) async {

    SharedPreferences preferences = await SharedPreferences.getInstance();
    print(preferences.getString('latitude'));
    print(preferences.getString('longitude'));
    setState(() {
      session = preferences.getString('Session');
    });
    final fetchNearbyEventApi =
        'https://home.eventeventapp.com/api/event/nearby?X-API-KEY=47d32cb10889cbde94e5f5f28ab461e52890034b&latitude=${preferences.getString('latitude')}&longitude=${preferences.getString('longitude')}&page=${page.toString()}';
    final response = await http.get(fetchNearbyEventApi, headers: {
      'Authorization': "Basic YWRtaW46MTIzNA==",
      'cookie': session
    });

    print(response.body);

    if (response.statusCode == 200) {
      setState(() {
        var extractedData = json.decode(response.body);
        nearbyEventData = extractedData['data'];
        page += 1;
      });
    }
  }
}
