import 'dart:convert';

import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class MyTicketWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyTicketWidgetState();
  }
}

class _MyTicketWidgetState extends State<MyTicketWidget> {
  var session;
  String userId;

  Map<String, dynamic> ticketData;
  Map<String, dynamic> publicData;

  List ticketDetailData;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataTicket();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ticketDetailData == null ? CircularProgressIndicator() : Container(
      color: Colors.black.withOpacity(0.05),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: ticketDetailData == null ? 0 : ticketDetailData.length,
          itemBuilder: (BuildContext, i) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 180,
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 150,
                              width: 100,
                              child: ticketDetailData[i]['ticket_image']
                                          ['url'] ==
                                      null
                                  ? Image.asset('assets/white.png')
                                  : Image.network(
                                      ticketDetailData[i]['ticket_image']
                                          ['url'],
                                      fit: BoxFit.fill,
                                    ),
                            ),
                            SizedBox(
                              width: 25,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  ticketDetailData[i]['event']['dateStart'] ==
                                          null
                                      ? 'loading'
                                      : ticketDetailData[i]['event']
                                          ['dateStart'],
                                  style: TextStyle(color: eventajaGreenTeal),
                                ),
                                Container(
                                    width: 180,
                                    child: Text(
                                      ticketDetailData[i]['ticket']
                                                  ['ticket_name'] ==
                                              null
                                          ? 'loading'
                                          : ticketDetailData[i]['ticket']
                                              ['ticket_name'],
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    )),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  ticketDetailData[i]['ticket_code'] == null
                                      ? 'loading'
                                      : ticketDetailData[i]['ticket_code'],
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(ticketDetailData[i]['event']
                                                ['timeStart'] ==
                                            null &&
                                        ticketDetailData[i]['event']
                                                ['timeEnd'] ==
                                            null
                                    ? 'loading'
                                    : ticketDetailData[i]['event']
                                            ['timeStart'] +
                                        ' - ' + ticketDetailData[i]['event']['timeEnd']),
                                SizedBox(height: 15),
                                Container(
                                  width: 180,
                                  child: Text(
                                      ticketDetailData[i]['event']['name'] == null ? 'loading' : ticketDetailData[i]['event']['name'],
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            );
          }),
    );
  }

  Future getDataTicket() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      session = prefs.getString('Session');
      userId = prefs.getString('Last User ID');
    });

    var urlApi =
        BaseApi().apiUrl + '/tickets/all?X-API-KEY=${API_KEY}&page=1&search=';
    final response = await http.get(urlApi, headers: {
      'Authorization': 'Basic YWRtaW46MTIzNA==',
      'cookie': session
    });

    if (response.statusCode == 200) {
      var extractedData = json.decode(response.body);
      ticketDetailData = extractedData['data'];
    }
  }
}
