import 'package:belajar_mvvm/helper/API/apiHelper.dart';
import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsWidget extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SettingsWidgetState();
  }
}

class _SettingsWidgetState extends State<SettingsWidget>{

  Future setSharedPreferencesToEmpty() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getKeys());
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.05),
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Icon(CupertinoIcons.back,
            size: 50,
            color: eventajaGreenTeal,
          ),
        ),
        title: Text('SETTINGS', style: TextStyle(color: eventajaGreenTeal),),
        centerTitle: true,
      ),
      body: GestureDetector(
        onTap: (){
          setSharedPreferencesToEmpty();
          requestLogout(context);
        },
              child: Container(
          width: MediaQuery.of(context).size.width,
          height: 50,
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.only(left: 30, bottom: 10, top: 18),
            child: Text('Log Out', style: TextStyle(color: Colors.red, fontSize: 20),),
          ),
        ),
      ),
    );
  }
}