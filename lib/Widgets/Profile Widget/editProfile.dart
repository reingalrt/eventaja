import 'package:belajar_mvvm/helper/colorsManagement.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditProfileWidget extends StatefulWidget {
  final String profileImage;
  final String username;
  final String firstName;
  final String lastName;
  final String dateOfBirth;
  final String bio;
  final String email;
  final String phone;
  final String website;

  const EditProfileWidget(
      {Key key,
      this.profileImage,
      this.username,
      this.firstName,
      this.lastName,
      this.dateOfBirth,
      this.bio,
      this.email,
      this.phone,
      this.website})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EditProfileWidgetState();
  }
}

class _EditProfileWidgetState extends State<EditProfileWidget>
    with AutomaticKeepAliveClientMixin<EditProfileWidget> {

  TextEditingController usernameController;
  TextEditingController firstNameController;
  TextEditingController lastNameController;
  TextEditingController emailController;
  TextEditingController shortBioController;
  TextEditingController phoneController;
  TextEditingController websiteController;

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.05),
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        leading: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              CupertinoIcons.back,
              color: eventajaGreenTeal,
              size: 50,
            )),
        centerTitle: true,
        title: Text(
          'EDIT PROFILE',
          style: TextStyle(color: eventajaGreenTeal),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: Center(
              child: Text(
                'SAVE',
                style: TextStyle(fontSize: 20, color: eventajaGreenTeal),
              ),
            ),
          )
        ],
      ),
      body: buildMainView(),
    );
  }

  Widget buildMainView() {
    return ListView(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        profilePicture(),
        SizedBox(
          height: 30,
        ),
        basicSettings(),
        SizedBox(
          height: 20,
        ),
        contactSettings(),
        SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget profilePicture() {
    return GestureDetector(
      child: Column(
        children: <Widget>[
          Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: widget.profileImage == null ? AssetImage('assets/white.png') : NetworkImage(widget.profileImage),
                    fit: BoxFit.contain)),
          ),
          SizedBox(
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  CupertinoIcons.photo_camera_solid,
                  color: eventajaGreenTeal,
                  size: 30,
                ),
                Text(
                  'Tap to change photo',
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget basicSettings() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          'BASIC SETTINGS',
          style: TextStyle(
              fontSize: 15, fontWeight: FontWeight.bold, color: Colors.grey),
        ),
        SizedBox(
          height: 15,
        ),
        Container(
          height: 200,
          width: MediaQuery.of(context).size.width / 1.2,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey, offset: Offset(1, 1), blurRadius: 2)
              ]),
          child: Padding(
            padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Username', style: TextStyle(color: eventajaGreenTeal, fontSize: 15, fontWeight: FontWeight.bold),),
                    SizedBox(height: 20,),
                    Text('First Name', style: TextStyle(color: eventajaGreenTeal, fontSize: 15, fontWeight: FontWeight.bold),),
                    SizedBox(height: 20,),
                    Text('Last Name', style: TextStyle(color: eventajaGreenTeal, fontSize: 15, fontWeight: FontWeight.bold),),
                    SizedBox(height: 20,),
                    Text('Date Of Birth', style: TextStyle(color: eventajaGreenTeal, fontSize: 15, fontWeight: FontWeight.bold),),
                    SizedBox(height: 20,),
                    Text('Bio', style: TextStyle(color: eventajaGreenTeal, fontSize: 15, fontWeight: FontWeight.bold),),
                  ],),
                SizedBox(
                  width: 25,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: 150,
                        child: TextFormField(
                          controller: usernameController,
                          initialValue: widget.username == null ? '' : widget.username,
                          textAlign: TextAlign.start,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(vertical: 1),
                            hintText: 'Username',
                            hintStyle: TextStyle(fontSize: 15),
                          ),
                        ),
                      ),
                    SizedBox(height: 15,),
                    Container(
                        width: 150,
                        child: TextFormField(
                          controller: firstNameController,
                          initialValue: widget.firstName == null ? '' : widget.firstName,
                          textAlign: TextAlign.start,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 1),
                              border: InputBorder.none,
                              hintText: 'First Name',
                              hintStyle: TextStyle(fontSize: 15)
                          ),
                        ),
                      ),
                    SizedBox(height: 20,),
                    Container(
                        width: 150,
                        child: TextFormField(
                          controller: lastNameController,
                          initialValue: widget.lastName == null ? '' : widget.lastName,
                          textAlign: TextAlign.start,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 1),
                              border: InputBorder.none,
                              hintText: 'Last Name',
                              hintStyle: TextStyle(fontSize: 15)
                          ),
                        ),
                      ),
                    SizedBox(height: 15,),
                    Container(
                        width: 150,
                        child: TextFormField(
                          textAlign: TextAlign.start,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 1),
                              border: InputBorder.none,
                              hintText: 'Birthday',
                              hintStyle: TextStyle(fontSize: 15)
                          ),
                        ),
                      ),
                    SizedBox(height: 15,),
                    Container(
                        width: 150,
                        child: TextFormField(
                          initialValue: widget.bio == null ? '' : widget.bio,
                          controller: shortBioController,
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          textAlign: TextAlign.start,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 1),
                              border: InputBorder.none,
                              hintText: 'Short Bio',
                              hintStyle: TextStyle(fontSize: 15)
                          ),
                        ),
                      ),
                  ],)
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget contactSettings() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          'CONTACT SETTINGS',
          style: TextStyle(
              fontSize: 15, fontWeight: FontWeight.bold, color: Colors.grey),
        ),
        SizedBox(
          height: 15,
        ),
        Container(
          height: 120,
          width: MediaQuery.of(context).size.width / 1.2,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey, offset: Offset(1, 1), blurRadius: 2)
              ]),
          child: Padding(
            padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Email', style: TextStyle(color: eventajaGreenTeal, fontSize: 15, fontWeight: FontWeight.bold),),
                    SizedBox(height: 20,),
                    Text('Phone', style: TextStyle(color: eventajaGreenTeal, fontSize: 15, fontWeight: FontWeight.bold),),
                    SizedBox(height: 20,),
                    Text('Website', style: TextStyle(color: eventajaGreenTeal, fontSize: 15, fontWeight: FontWeight.bold),),
                  ],),
                SizedBox(
                  width: 57,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 150,
                      child: TextFormField(
                        controller: emailController,
                        initialValue: widget.email == null ? '' : widget.email,
                        textAlign: TextAlign.start,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(vertical: 1),
                          hintText: 'Email Address',
                          hintStyle: TextStyle(fontSize: 15),
                        ),
                      ),
                    ),
                    SizedBox(height: 15,),
                    Container(
                      width: 150,
                      child: TextFormField(
                        controller: phoneController,
                        initialValue: widget.phone == null ? '' : widget.phone,
                        textAlign: TextAlign.start,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 1),
                            border: InputBorder.none,
                            hintText: 'Phone Number',
                            hintStyle: TextStyle(fontSize: 15)
                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      width: 150,
                      child: TextFormField(
                        controller: websiteController,
                        initialValue: widget.website == null ? '' : widget.website,
                        textAlign: TextAlign.start,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 1),
                            border: InputBorder.none,
                            hintText: 'Website',
                            hintStyle: TextStyle(fontSize: 15)
                        ),
                      ),
                    ),
                  ],)
              ],
            ),
          ),
        ),
      ],
    );
  }
}
