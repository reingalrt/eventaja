import 'package:belajar_mvvm/Widgets/Profile%20Widget/SettingsWidget.dart';
import 'package:belajar_mvvm/Widgets/Profile%20Widget/editProfile.dart';
import 'package:belajar_mvvm/Widgets/RecycleableWidget/listviewWithAppBar.dart';
import 'package:belajar_mvvm/helper/API/baseApi.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'MyTicketWidget.dart';

class ProfileHeader extends StatefulWidget {
  final String currentUserId;
  final String username;
  final String fullName;
  final String firstName;
  final String email;
  final String website;
  final String phone;
  final String lastName;
  final String profilePhotoURL;
  final String eventCreatedCount;
  final String eventGoingCount;
  final String following;
  final String follower;
  final String bio;

  const ProfileHeader(
      {Key key,
      this.currentUserId,
      this.username,
      this.fullName,
      this.lastName,
      this.profilePhotoURL,
      this.eventCreatedCount,
      this.eventGoingCount,
      this.following,
      this.follower, this.firstName, this.email, this.website, this.phone, this.bio})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfileHeaderState();
  }
}

class _ProfileHeaderState extends State<ProfileHeader>
    with AutomaticKeepAliveClientMixin<ProfileHeader> {
  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        profileHeader(context),
        profileDetails(context, widget)
      ],
    ));
  }
}

Widget profileHeader(BuildContext context) {
  return Container(
    color: Colors.transparent,
    child: Container(
      height: 70,
      width: MediaQuery.of(context).size.width,
      color: Colors.grey,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 300,
            ),
            GestureDetector(
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => SettingsWidget()));
              },
              child: Icon(
                CupertinoIcons.gear_solid,
                color: Colors.white,
                size: 35,
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

handleClickButtonEditProfile(BuildContext context, ProfileHeader widget) {
  Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => EditProfileWidget(
            username: widget.username,
            firstName: widget.fullName,
            lastName: widget.lastName,
            profileImage: widget.profilePhotoURL,
            email: widget.email,
            phone: widget.phone,
            website: widget.website,
        bio: widget.bio,
          )
        )
      );
}

Widget profileDetails(BuildContext context, ProfileHeader widget) {
  return Container(
    child: Expanded(
      child: ListView(
        children: <Widget>[
          Stack(
            fit: StackFit.passthrough,
            children: <Widget>[
              UnconstrainedBox(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 150,
                  width: MediaQuery.of(context).size.width,
                  child: Image(image: widget.profilePhotoURL == null ? AssetImage('assets/white.png') : NetworkImage(widget.profilePhotoURL), fit: BoxFit.cover,),
                ),
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 200,
                      width: 200,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5,
                                offset: Offset(1, 1))
                          ],
                          image: DecorationImage(
                              image: widget.profilePhotoURL == null
                                  ? AssetImage('assets/white.png')
                                  : CachedNetworkImageProvider(
                                      widget.profilePhotoURL),
                              fit: BoxFit.cover)),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      widget.fullName == null ? 'loading' : widget.fullName,
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      widget.username == null ? 'loading' : '@' + widget.username,
                      style: TextStyle(fontSize: 15, color: Colors.grey),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        handleClickButtonEditProfile(context, widget);
                      },
                      child: Container(
                        height: 50,
                        width: 100,
                        child: Image.asset('assets/icons/btn_edit_profile.png'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                widget.eventCreatedCount == null
                                    ? '0'
                                    : widget.eventCreatedCount,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: widget.eventCreatedCount == "0" ||
                                            widget.eventCreatedCount == null
                                        ? Colors.grey
                                        : Colors.black),
                              ),
                              Text('EVENT CREATED',
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: widget.eventCreatedCount == "0" ||
                                              widget.eventCreatedCount == null
                                          ? Colors.grey
                                          : Colors.black))
                            ],
                          ),
                          SizedBox(
                            width: 25,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                  widget.eventGoingCount == null
                                      ? '0'
                                      : widget.eventGoingCount,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: widget.eventGoingCount == "0" ||
                                              widget.eventGoingCount == null
                                          ? Colors.grey
                                          : Colors.black)),
                              Text('EVENT GOING',
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: widget.eventGoingCount == "0" ||
                                              widget.eventGoingCount == null
                                          ? Colors.grey
                                          : Colors.black))
                            ],
                          ),
                          SizedBox(
                            width: 25,
                          ),
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) => ListViewWithAppBar(
                                        title: 'FOLLOWER',
                                        apiURL: BaseApi().apiUrl + '/user/follower?X-API-KEY=${API_KEY}&userID=${widget.currentUserId}&page=1',
                                      )
                                  )
                              );
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                    widget.follower == null ? '0' : widget.follower,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: widget.follower == "0" ||
                                                widget.follower == null
                                            ? Colors.grey
                                            : Colors.black)),
                                Text('FOLLOWER',
                                    style: TextStyle(
                                        fontSize: 10,
                                        color: widget.follower == "0" ||
                                                widget.follower == null
                                            ? Colors.grey
                                            : Colors.black))
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 25,
                          ),
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) => ListViewWithAppBar(
                                        title: 'FOLLOWING',
                                        apiURL: BaseApi().apiUrl + '/user/following?X-API-KEY=${API_KEY}&userID=${widget.currentUserId}&page=1',
                                      )
                                  )
                              );
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                    widget.following == null
                                        ? '0'
                                        : widget.following,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: widget.following == "0" ||
                                                widget.following == null
                                            ? Colors.grey
                                            : Colors.black)),
                                Text('FOLLOWING',
                                    style: TextStyle(
                                        fontSize: 10,
                                        color: widget.following == "0"
                                            ? Colors.grey
                                            : Colors.black))
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    tabNavigator(context)
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}

Widget tabNavigator(BuildContext context) {
  return DefaultTabController(
    length: 2,
    initialIndex: 0,
    child: Column(
      children: <Widget>[
        TabBar(
          tabs: <Widget>[
            Tab(
              text: 'TIMELINE',
            ),
            Tab(text: 'MY TICKET')
          ],
        ),
        Container(
          height: MediaQuery.of(context).size.height,
          child: TabBarView(
            children: <Widget>[
              Container(),
              MyTicketWidget()
            ],
          ),
        ),
      ],
    ),
  );
}

Widget myTickets() {}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
